import java.util.*;
public class Compte {

    private long idCompte;
    private String type;
    private double solde;


    public Compte(String type, double solde)
    {
        this.type = type;
        this.solde = solde;
    }
    public void setIdCompte(long idCompte)
    {
        this.idCompte = idCompte;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public double getSolde()
    {
        return solde;
    }
    public void setSolde(double solde)
    {
        this.solde = solde;
    }
};
